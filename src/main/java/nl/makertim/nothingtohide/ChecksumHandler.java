package nl.makertim.nothingtohide;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextComponentTranslation;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.client.config.GuiConfigEntries;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.security.DigestInputStream;
import java.security.MessageDigest;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class ChecksumHandler implements IMessageHandler<ChecksumMessage, ChecksumMessage.Response> {

	public static Map<UUID, ICommandSender> senderMap = new HashMap<>();

	@Override
	public ChecksumMessage.Response onMessage(ChecksumMessage message, MessageContext ctx) {
		if (message instanceof ChecksumMessage.Request) {
			ChecksumMessage.Request request = (ChecksumMessage.Request) message;
			String path = request.getPath().replaceAll("\\.\\./", "").replaceAll("\\.\\.\\\\", "");
			File file = new File(".", path);
			if (!file.exists()) {
				NothingToHideMod.NETWORK_WRAPPER.sendToServer(new ChecksumMessage.Response("no such file", "ERROR", request.getUuid()));
				return null;
			}
			if (file.isDirectory()) {
				NothingToHideMod.NETWORK_WRAPPER.sendToServer(new ChecksumMessage.Response("file is directory", "ERROR", request.getUuid()));
				return null;
			}

			String md5 = md5Of(file);
			String sha = shaOf(file);

			NothingToHideMod.NETWORK_WRAPPER.sendToServer(new ChecksumMessage.Response(md5, sha, request.getUuid()));
		} else if (message instanceof ChecksumMessage.Response) {
			ChecksumMessage.Response response = (ChecksumMessage.Response) message;
			ICommandSender sender = senderMap.get(response.getUuid());
			if (sender != null) {
				sender.sendMessage(new TextComponentString("Checksum"));
				sender.sendMessage(new TextComponentString("  MD5: ")
						.appendSibling(new TextComponentString(response.getMd5())
								.setStyle(new Style().setColor(TextFormatting.GREEN))));
				sender.sendMessage(new TextComponentString("  SHA: ")
						.appendSibling(new TextComponentString(response.getSha1())
								.setStyle(new Style().setColor(TextFormatting.GREEN))));
			} else {
				System.out.println("MD5: " + response.getMd5());
				System.out.println("SHA: " + response.getSha1());
			}
		}
		return null;
	}

	private static String hexOf(byte[] bytes) {
		StringBuilder result = new StringBuilder();
		for (byte aByte : bytes) {
			result.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
		}
		return result.toString();
	}

	private static String md5Of(File file) {
		return hexOf(encryptOf("MD5", file));
	}

	private static String shaOf(File file) {
		return hexOf(encryptOf("SHA-1", file));
	}

	private static byte[] encryptOf(String digest, File file) {
		byte[] ret = new byte[0];
		try {
			InputStream fis = new FileInputStream(file);
			byte[] buffer = new byte[1024];
			MessageDigest complete = MessageDigest.getInstance(digest);
			int numRead;
			do {
				numRead = fis.read(buffer);
				if (numRead > 0) {
					complete.update(buffer, 0, numRead);
				}
			} while (numRead != -1);

			fis.close();
			ret = complete.digest();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}
}
