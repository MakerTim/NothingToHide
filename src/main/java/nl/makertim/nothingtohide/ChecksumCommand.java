package nl.makertim.nothingtohide;

import net.minecraft.command.ICommandSender;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;

public class ChecksumCommand extends FileCommand {

	@Override
	public String getName() {
		return "checksum";
	}

	@Override
	protected void execute(MinecraftServer server, ICommandSender sender, EntityPlayerMP target, String[] args) {
		ChecksumMessage message = new ChecksumMessage.Request(stitch(args, 1));
		ChecksumHandler.senderMap.put(message.getUuid(), sender);
		NothingToHideMod.NETWORK_WRAPPER.sendTo(message, target);
	}
}
