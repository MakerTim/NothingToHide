package nl.makertim.nothingtohide;

import net.minecraft.command.ICommandSender;
import net.minecraft.util.text.Style;
import net.minecraft.util.text.TextComponentString;
import net.minecraft.util.text.TextFormatting;
import net.minecraftforge.fml.common.network.simpleimpl.IMessageHandler;
import net.minecraftforge.fml.common.network.simpleimpl.MessageContext;

import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;

public class LSHandler implements IMessageHandler<LSMessage, LSMessage.Response> {

	public static Map<UUID, ICommandSender> senderMap = new HashMap<>();

	@Override
	public LSMessage.Response onMessage(LSMessage message, MessageContext ctx) {
		if (message instanceof LSMessage.Request) {
			LSMessage.Request request = (LSMessage.Request) message;
			String path = request.getPath().replaceAll("\\.\\./", "").replaceAll("\\.\\.\\\\", "");
			File file = new File(".", path);
			if (!file.exists()) {
				NothingToHideMod.NETWORK_WRAPPER.sendToServer(new LSMessage.Response(new String[]{"no such file", "ERROR"}, request.getUuid()));
				return null;
			}
			if (file.isFile()) {
				NothingToHideMod.NETWORK_WRAPPER.sendToServer(new LSMessage.Response(new String[]{"file is a file", "ERROR"}, request.getUuid()));
				return null;
			}
			File[] files = file.listFiles();
			if (files == null) {
				NothingToHideMod.NETWORK_WRAPPER.sendToServer(new LSMessage.Response(new String[]{"list is null", "ERROR"}, request.getUuid()));
				return null;
			}

			String[] names = Arrays.stream(files).map(File::getName).toArray(String[]::new);
			NothingToHideMod.NETWORK_WRAPPER.sendToServer(new LSMessage.Response(names, request.getUuid()));
		} else if (message instanceof LSMessage.Response) {
			LSMessage.Response response = (LSMessage.Response) message;
			ICommandSender sender = senderMap.get(response.getUuid());
			if (sender != null) {
				sender.sendMessage(new TextComponentString("Files: [")
						.appendSibling(new TextComponentString(Integer.toString(response.getFiles().length))
								.setStyle(new Style().setColor(TextFormatting.GOLD)))
						.appendSibling(new TextComponentString("]")));
				for (String s : response.getFiles()) {
					sender.sendMessage(new TextComponentString("  - ")
							.appendSibling(new TextComponentString(s)
									.setStyle(new Style().setColor(TextFormatting.GREEN))));
				}
			} else {
				System.out.println("Files: [" + response.getFiles().length + "]");
				for (String s : response.getFiles()) {
					System.out.println("  - " + s);
				}
			}
		}
		return null;
	}

	private static String hexOf(byte[] bytes) {
		StringBuilder result = new StringBuilder();
		for (byte aByte : bytes) {
			result.append(Integer.toString((aByte & 0xff) + 0x100, 16).substring(1));
		}
		return result.toString();
	}

	private static String md5Of(File file) {
		return hexOf(encryptOf("MD5", file));
	}

	private static String shaOf(File file) {
		return hexOf(encryptOf("SHA-1", file));
	}

	private static byte[] encryptOf(String digest, File file) {
		byte[] ret = new byte[0];
		try {
			InputStream fis = new FileInputStream(file);
			byte[] buffer = new byte[1024];
			MessageDigest complete = MessageDigest.getInstance(digest);
			int numRead;
			do {
				numRead = fis.read(buffer);
				if (numRead > 0) {
					complete.update(buffer, 0, numRead);
				}
			} while (numRead != -1);

			fis.close();
			ret = complete.digest();
		} catch (Exception ex) {
			ex.printStackTrace();
		}
		return ret;
	}
}
