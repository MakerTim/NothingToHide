package nl.makertim.nothingtohide;

import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.common.Mod.EventHandler;
import net.minecraftforge.fml.common.event.FMLInitializationEvent;
import net.minecraftforge.fml.common.event.FMLPreInitializationEvent;
import net.minecraftforge.fml.common.event.FMLServerStartingEvent;
import net.minecraftforge.fml.common.network.NetworkRegistry;
import net.minecraftforge.fml.common.network.simpleimpl.SimpleNetworkWrapper;
import net.minecraftforge.fml.relauncher.Side;
import org.apache.logging.log4j.Logger;

import java.net.URISyntaxException;

@Mod(modid = NothingToHideMod.MODID, name = NothingToHideMod.NAME, version = NothingToHideMod.VERSION)
public class NothingToHideMod {
	public static final String MODID = "nothingtohide";
	public static final String NAME = "Nothing To Hide";
	public static final String VERSION = "1.0";

	public static final SimpleNetworkWrapper NETWORK_WRAPPER = NetworkRegistry.INSTANCE.newSimpleChannel(MODID);

	private static Logger logger;

	@EventHandler
	public void preInit(FMLPreInitializationEvent event) {
		logger = event.getModLog();
		MinecraftForge.EVENT_BUS.register(this);
	}

	@EventHandler
	public void init(FMLInitializationEvent event) {
		int i = 0;
		ChecksumHandler handlerChecksum = new ChecksumHandler();
		NETWORK_WRAPPER.registerMessage(handlerChecksum, ChecksumMessage.Request.class, i++, Side.SERVER);
		NETWORK_WRAPPER.registerMessage(handlerChecksum, ChecksumMessage.Request.class, i++, Side.CLIENT);
		NETWORK_WRAPPER.registerMessage(handlerChecksum, ChecksumMessage.Response.class, i++, Side.SERVER);
		NETWORK_WRAPPER.registerMessage(handlerChecksum, ChecksumMessage.Response.class, i++, Side.CLIENT);
		LSHandler handlerLs = new LSHandler();
		NETWORK_WRAPPER.registerMessage(handlerLs, LSMessage.Request.class, i++, Side.SERVER);
		NETWORK_WRAPPER.registerMessage(handlerLs, LSMessage.Request.class, i++, Side.CLIENT);
		NETWORK_WRAPPER.registerMessage(handlerLs, LSMessage.Response.class, i++, Side.SERVER);
		NETWORK_WRAPPER.registerMessage(handlerLs, LSMessage.Response.class, i++, Side.CLIENT);
	}

	@EventHandler
	public void startWorld(FMLServerStartingEvent event) {
		event.registerServerCommand(new ChecksumCommand());
		event.registerServerCommand(new LSCommand());
	}
}
