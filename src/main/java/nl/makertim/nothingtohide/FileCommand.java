package nl.makertim.nothingtohide;

import net.minecraft.command.*;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;
import java.io.File;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

public abstract class FileCommand extends CommandBase {


	@Override
	public String getUsage(ICommandSender sender) {
		if (sender.canUseCommand(4, getName())) {
			return "/" + getName() + " [username] path/to/file";
		}
		return "/" + getName();
	}

	@Override
	public void execute(MinecraftServer server, ICommandSender sender, String[] args) throws CommandException {
		if (args.length == 0) {
			throw new WrongUsageException("2 argument needed");
		} else if (args.length == 1) {
			throw new WrongUsageException("1 more argument needed");
		}
		EntityPlayerMP playerMP = server.getPlayerList().getPlayerByUsername(args[0]);
		if (playerMP == null) {
			throw new PlayerNotFoundException(args[0]);
		}

		execute(server, sender, playerMP, args);
	}

	protected abstract void execute(MinecraftServer server, ICommandSender sender, EntityPlayerMP target, String[] args);

	public String stitch(String[] args, int from) {
		StringBuilder ret = new StringBuilder();
		for (int i = from; i < args.length; i++) {
			ret.append(args[i]).append(" ");
		}
		ret.deleteCharAt(ret.length() - 1);
		return ret.toString();
	}

	@Override
	public List<String> getTabCompletions(MinecraftServer server, ICommandSender sender, String[] args, @Nullable BlockPos targetPos) {
		switch (args.length) {
			case 0:
			case 1:
				return getListOfStringsMatchingLastWord(args, Arrays.asList(server.getOnlinePlayerNames()));
			case 2:
				File[] folders = new File(".").listFiles();
				String[] list;
				if (folders == null) {
					list = new String[]{"/", "/mods", "/config", "/resourcepacks"};
				} else {
					list = Arrays.stream(folders).map(File::getName).toArray(String[]::new);
				}
				return getListOfStringsMatchingLastWord(args, Arrays.asList(list));
			default:
				return Collections.emptyList();
		}
	}
}
