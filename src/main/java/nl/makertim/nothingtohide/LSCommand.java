package nl.makertim.nothingtohide;

import net.minecraft.command.*;
import net.minecraft.entity.player.EntityPlayerMP;
import net.minecraft.server.MinecraftServer;
import net.minecraft.util.math.BlockPos;

import javax.annotation.Nullable;
import java.io.File;
import java.util.*;

public class LSCommand extends FileCommand {

	@Override
	public String getName() {
		return "ls";
	}

	@Override
	protected void execute(MinecraftServer server, ICommandSender sender, EntityPlayerMP target, String[] args) {
		LSMessage message = new LSMessage.Request(stitch(args, 1));
		LSHandler.senderMap.put(message.getUuid(), sender);
		NothingToHideMod.NETWORK_WRAPPER.sendTo(message, target);
	}
}
