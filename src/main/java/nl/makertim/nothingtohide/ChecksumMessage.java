package nl.makertim.nothingtohide;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

import java.util.UUID;

public abstract class ChecksumMessage implements IMessage {

	private UUID uuid = UUID.randomUUID();

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		uuid = UUID.fromString(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, uuid.toString());
	}

	public static class Request extends ChecksumMessage {
		private String path = "";

		public Request() {
		}

		public Request(String path) {
			this.path = path;
		}

		@Override
		public void fromBytes(ByteBuf buf) {
			super.fromBytes(buf);
			path = ByteBufUtils.readUTF8String(buf);
		}

		@Override
		public void toBytes(ByteBuf buf) {
			super.toBytes(buf);
			ByteBufUtils.writeUTF8String(buf, path);
		}

		public String getPath() {
			return path;
		}
	}

	public static class Response extends ChecksumMessage {
		private String md5 = "";
		private String sha1 = "";

		public Response() {
		}

		public Response(String md5, String sha1, UUID uuid) {
			this.md5 = md5;
			this.sha1 = sha1;
			setUuid(uuid);
		}

		@Override
		public void fromBytes(ByteBuf buf) {
			super.fromBytes(buf);
			md5 = ByteBufUtils.readUTF8String(buf);
			sha1 = ByteBufUtils.readUTF8String(buf);
		}

		@Override
		public void toBytes(ByteBuf buf) {
			super.toBytes(buf);
			ByteBufUtils.writeUTF8String(buf, md5);
			ByteBufUtils.writeUTF8String(buf, sha1);
		}

		public String getMd5() {
			return md5;
		}

		public String getSha1() {
			return sha1;
		}
	}


}
