package nl.makertim.nothingtohide;

import io.netty.buffer.ByteBuf;
import net.minecraftforge.fml.common.network.ByteBufUtils;
import net.minecraftforge.fml.common.network.simpleimpl.IMessage;

import java.util.UUID;

public abstract class LSMessage implements IMessage {

	private UUID uuid = UUID.randomUUID();

	public UUID getUuid() {
		return uuid;
	}

	public void setUuid(UUID uuid) {
		this.uuid = uuid;
	}

	@Override
	public void fromBytes(ByteBuf buf) {
		uuid = UUID.fromString(ByteBufUtils.readUTF8String(buf));
	}

	@Override
	public void toBytes(ByteBuf buf) {
		ByteBufUtils.writeUTF8String(buf, uuid.toString());
	}

	public static class Request extends LSMessage {
		private String path = "";

		public Request() {
		}

		public Request(String path) {
			this.path = path;
		}

		@Override
		public void fromBytes(ByteBuf buf) {
			super.fromBytes(buf);
			path = ByteBufUtils.readUTF8String(buf);
		}

		@Override
		public void toBytes(ByteBuf buf) {
			super.toBytes(buf);
			ByteBufUtils.writeUTF8String(buf, path);
		}

		public String getPath() {
			return path;
		}
	}

	public static class Response extends LSMessage {
		private String[] files = new String[0];

		public Response() {
		}

		public Response(String[] files, UUID uuid) {
			this.files = files;
			setUuid(uuid);
		}

		@Override
		public void fromBytes(ByteBuf buf) {
			super.fromBytes(buf);
			int size = buf.readInt();
			files = new String[size];
			for (int i = 0; i < size; i++) {
				files[i] = ByteBufUtils.readUTF8String(buf);
			}
		}

		@Override
		public void toBytes(ByteBuf buf) {
			super.toBytes(buf);
			buf.writeInt(files.length);
			for (String file : files) {
				ByteBufUtils.writeUTF8String(buf, file);
			}
		}

		public String[] getFiles() {
			return files;
		}
	}


}
